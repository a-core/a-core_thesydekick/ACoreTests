.. autoclass:: acoretests.generic_tests.GenericTests
    :members:

.. autoclass:: acoretests.acoretests_cli.ACoreTestsCLI
    :members:
