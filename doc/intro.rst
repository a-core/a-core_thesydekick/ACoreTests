A-Core Tests
============

This entity provides a command-line interface (CLI) for running software programs on A-Core. There are two main parts:

- :class:`GenericTests<acoretests.generic_tests.GenericTests>`
- :class:`ACoreTestsCLI<acoretests.acoretests_cli.ACoreTestsCLI>`

The interface starts from :class:`ACoreTestsCLI<acoretests.acoretests_cli.ACoreTestsCLI>`, which holds the command-line interface. It is included in :class:`GenericTests<acoretests.generic_tests.GenericTests>` to compose a callable Python program. Run

.. code-block:: bash

   python3 generic_tests.py --help

to find the help string for the interface.

:class:`GenericTests<acoretests.generic_tests.GenericTests>` runs single or multiple tests, and composes the results. It has some additional features, such as overriding the sim config from command line, or executing only selfchecking tests. However, to emphasize, **this entity is not needed to run A-Core on a custom setup**. It is purely a convenience entity.

API
---

.. toctree::
   :maxdepth: 2

   api