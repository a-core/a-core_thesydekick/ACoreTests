import os
import sys

thesdk_path = os.path.abspath(os.path.join(os.path.dirname(__file__),'../../thesdk'))
if not thesdk_path in sys.path:
    sys.path.append(thesdk_path)

from thesdk import *
import os

class ACoreTests(thesdk):
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self,*arg, **kwargs): 
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 

    def init(self):
        pass

    def run(self):
        pass
