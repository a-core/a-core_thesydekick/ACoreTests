#! python3
import os
import sys

thesdk_path = os.path.abspath(os.path.join(os.path.dirname(__file__),'../../thesdk'))
if not thesdk_path in sys.path:
    sys.path.append(thesdk_path)

from thesdk import *
from acorechip.config import ACoreTestConfig
from importlib import import_module
from acoretestbenches.common import TestFailError
from acoretests.acoretests_cli import ACoreTestsCLI
import glob

class GenericTests(thesdk):
    """Generic Test entity for A-Core. Called from command line.
    Runs all tests provided in ``test_configs``,
    composes results, throws an error if any test fails.

    Example
    -------
    .. code-block:: 

        python3 generic_tests.py --help
    
    Parameters
    ----------

    test_configs : list
        List of paths to test configs. Wildcards supported.

    only_selfchecking : bool
        Filter out all tests that have not been marked as selfchecking

    allowed_to_fail : list
        List of test names that are allowed to fail. By default, test name is the base name of test config.
        The test result will be visible in the results, but it does not throw an error.

    sim_config : str
        Path to sim_config. If left empty, the sim_config defined in the test_config will be used.

    
    """

    # ANSI escape codes for terminal colors
    CEND = "\33[0m"
    CRED = "\33[31m"
    CGREEN = "\33[32m"

    def __init__(self, **kwargs):
        self.print_log(type='I', msg='Inititalizing %s' %(__name__))
        self.config_loader = ACoreTestConfig()
        self._test_configs = []

    def init_props(self, **kwargs):
        """
        Initialize properties that come from CLI. `sim_config` argument is used to replace
        `sim_config_yaml` in all `test_configs`.
        """
        self.test_configs = kwargs.get("test_configs", None)
        self.only_selfchecking = kwargs.get("only_selfchecking", False)
        self.allowed_to_fail = kwargs.get("allowed_to_fail", [])
        self.sim_config = kwargs.get("sim_config", None)
        if self.sim_config:
            if not os.path.isabs(self.sim_config):
                self.sim_config = os.path.abspath(self.sim_config)
            self.config_loader.replace_list = {"sim_config_yaml": self.sim_config}

    def parse_configs(self):
        """
        Collect all provided test config paths into a list. The paths are collected into
        ``self._test_configs``.

        This function also checks for certain conditions, such as `selfchecking`. If the program was run with
        `only_selfchecking` option, those tests that have ``selfchecking: false`` are filtered out.
        """
        for tc in self.test_configs:
            # Convert all paths to absolute
            tc_u = tc
            # Relative paths are relative to the entity
            if not os.path.isabs(tc):
                tc_u = os.path.abspath(tc)
            # Expand wildcards
            exd_tcs = glob.glob(tc_u)
            if exd_tcs == []:
                self.print_log(type='W', msg=f"A nonexistent test config was given! {tc}")
            # Add to list only if conditions match
            # In case there are multiple conditions, only one of them being False
            # is enough to make this variable False
            for exd_tc in exd_tcs:
                add_to_list = True
                if not os.path.isfile(exd_tc):
                    self.print_log(type='E', msg=f'A nonexistent test config was given! {exd_tc}')
                    continue
                self.config_loader.config_path = exd_tc
                self.config_loader.init()
                self.config_loader.run()
                if self.only_selfchecking:
                    add_to_list &= self.config_loader.config["sim_config"]["selfchecking"]
                if add_to_list:
                    self._test_configs += [exd_tc]
            

    def init(self, **kwargs):
        """
        Initialize everything. Testbenches are stored into ``self.tbs``. It runs ACoreTestConfig, and imports dynamically the correct testbench class.
        It runs the ``init`` function for the selected testbench. After this, the testbenches are stored in ``self.tbs``.
        """
        self.init_props(**kwargs)
        self.parse_configs()
        self.tbs = []
        for test_config_path in self._test_configs:
            config_loader = ACoreTestConfig(config_path=test_config_path)
            # Replace sim_config if provided
            if self.sim_config:
                if not os.path.isabs(self.sim_config):
                    self.sim_config = os.path.abspath(self.sim_config)
                config_loader.replace_list = {"sim_config_yaml": self.sim_config}
            config_loader.run()
            test_config = config_loader.config
            test_name = os.path.splitext(os.path.basename(test_config_path))[0]
            sim_config = test_config["sim_config"]
            tb_module = sim_config.get("tb_module", "acoretestbenches.generic_sim_testbenches")
            tb_class = sim_config.get("tb_class", "GenericTheSydekickSimTestbench")
            tb_module = import_module(tb_module)
            tb_class_ = getattr(tb_module, tb_class)
            tb = tb_class_(test_config=test_config, test_name=test_name)
            tb.acore_lib_path = os.path.join(thesdk.HOME, "Entities/acoresoftware/lib/a-core-library")
            tb.init()
            self.tbs.append(tb)

    def run_sequential(self):
        """
        Run testbenches sequentially
        """
        self.results = []
        for tb in self.tbs:
            base_name = tb.test_name
            self.print_log(type='I', msg=f"Running {base_name}.")
            try:
                tb.run()
                self.results.append([f"{base_name}", self.CGREEN + "PASS" + self.CEND, True])
            except TestFailError:
                self.results.append([f"{base_name}", self.CRED + "FAIL" + self.CEND, False])

    def run(self):
        """
        Run tests. Compose results, and print them after all tests have finished.

        Raises
        ------
        TestFailError
            If some test(s) failed
        """
        self.run_sequential()
        self.print_log(type="I", msg="Test results:")
        if self.allowed_to_fail:
            print("**********")
            print("Tests that are allowed to fail:")
            for test_name in self.allowed_to_fail:
                print(test_name)
            print("**********")

        result = True
        longest_name = len(max(self.results))
        for entry in self.results:
            if (not entry[2]) and (entry[0] not in self.allowed_to_fail):
                result = False
            print("{:{width}s} {:5s}".format(entry[0], entry[1], width=(longest_name)))

        if not result:
            raise TestFailError("Some test(s) failed!")


if __name__=="__main__":
    import argparse
    tests = GenericTests()
    cli = ACoreTestsCLI(name="python3 generic_tests.py")
    tests.init(**cli.kwargs)
    tests.run()
