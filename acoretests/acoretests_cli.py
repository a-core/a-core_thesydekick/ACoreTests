
"""
ACoreTestsCLI
=============


"""

import argparse

class ACoreTestsCLI():
    """
    Command-line interface for running tests for A-Core.

    Parameters
    ==========

    name : str
        Name of the program. This will show as the execution command in the help string.
    
    """
    def __init__(self, name):
        self.name = name
        self.build_cli()

    @property
    def cli_args(self) -> argparse.Namespace:
        """Parsed command line arguments"""
        return self._cli_args

    @property
    def kwargs(self) -> dict:
        """Parsed keyword arguments"""
        return self._kwargs

    def build_cli(self):
        """
        Build command-line interface.
        """
        description = (
            "Interface for running tests on A-Core."
        )
        example = (
            f"Example usage: {self.name} --test_configs ../../acoresoftware/test_configs/m-tests.yml\n"
        )
        parser = argparse.ArgumentParser(
            prog=self.name,
            description=description,
            epilog=example
        )
        parser.add_argument(
            "--test_configs",
            help="List of paths to test configs you want to run. Wildcards work.",
            type=str,
            nargs='*'
        )
        parser.add_argument(
            "--only_selfchecking",
            help="Select only selfchecking tests from test_configs",
            type=self.str2bool,
            default=False
        )
        parser.add_argument(
            "--allowed_to_fail",
            help="Tests allowed to fail. Filename only (without path).",
            type=str,
            nargs='*',
            default=[]
        )
        parser.add_argument(
            "--sim_config",
            help="Path to simulation config. If empty, the default one from test_config is used.",
            type=str,
            nargs='?',
            default=None,
        )
        self._cli_args = parser.parse_args()
        self._kwargs = vars(self.cli_args)

    def str2bool(self, v):
        """
        Helper for parsing boolean values
        """
        if isinstance(v, bool):
            return v
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')
