# ACoreTests

This entity contains a command line interface for executing various tests on various testbenches.

## Quickstart
Run `./configure && make help` for help on running tests on command line.


### [Documentation](https://a-core-risc-v.readthedocs.io/stable/)